<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->delete();
        $user = factory(User::class)->create([
            'email' => 'tom@example.net',
            'password' => 'secret',
        ]);
        echo 'Create user....' . PHP_EOL;
        echo 'email:' . $user->email . PHP_EOL;
        echo 'password:' . 'secret' . PHP_EOL;
    }
}
