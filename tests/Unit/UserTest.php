<?php

namespace Tests\Unit;

use App\Constants\HttpCodes;
use App\User;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Str;
use Tests\TestCase;

class UserTest extends TestCase
{
    use InteractsWithDatabase;
    use DatabaseTransactions;

    protected $url = "api/user";

    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * create new user
     * /api/user
     */
    public function testStoreRequestSuccess()
    {
        $user = factory(User::class)->make()->toArray();
        $user['password'] = Str::random(8);
        $this->post(
            $this->url,
            $user
        )->assertStatus(HttpCodes::SUCCESS)
            ->assertJson([
                "success" => true,
                "data" => [
                    "name" => $user["name"],
                    "email" => $user["email"],
                ]]);
        $this->assertDatabaseHas('users', [
            "name" => $user["name"],
            "email" => $user["email"],
        ]);
    }

    /**
     * get user
     * /api/user/{user}
     */
    public function testShowRequestSuccess()
    {

        $this->actingAs($this->user)->get(
            $this->url . '/' . $this->user->id,
            $this->headers
        )->assertStatus(HttpCodes::SUCCESS)
            ->assertJson([
                "success" => true,
                "data" => [
                    "name" => $this->user->name,
                    "email" => $this->user->email
                ]
            ]);
    }

    /**
     * update user
     * /api/user/{user}
     */
    public function testUpdateRequestSuccess()
    {
        $newName = factory(User::class)->make()->name;

        $this->actingAs($this->user)->put(
            $this->url . '/' . $this->user->id,
            ['name' => 'newName'],
            $this->headers
        )->assertStatus(HttpCodes::SUCCESS)
            ->assertJson([
                "success" => true,
                "data" => [
                    "name" => $newName,
                    "email" => $this->user->email
                ]
            ]);
    }

    /**
     * delete user
     * /api/user/{user}
     */
    public function testDestroyRequestSuccess()
    {
        $this->actingAs($this->user)->delete(
            $this->url . '/' . $this->user->id,
            [],
            $this->headers
        )->assertStatus(HttpCodes::SUCCESS)
            ->assertJson([
                "success" => true
            ]);
        $this->assertDatabaseMissing('users', [
            "email" => $this->user->email
        ]);
    }
}
