<?php

namespace Tests\Unit;

use App\Constants\HttpCodes;
use App\User;
use Illuminate\Support\Str;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * @var string
     */
    protected $url = "api/auth";

    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * success User login
     * /api/auth/login
     */
    public function testLoginRequestSuccess() {
        $this->post(
            $this->url . '/login',
            [
                'email' => $this->user->email,
                'password' => $this->password,
            ]
        )->assertStatus(HttpCodes::SUCCESS)
            ->assertJsonFragment([
                "success"=>true,
            ])
            ->assertJsonStructure([
                "success",
                "data" =>
                    ["access_token", "token_type","expires_in"]
            ]);
    }

    /**
     * failed User login
     * /api/auth/login
     */
    public function testLoginRequestFailed()
    {
        // incorrect password
        $this->post(
            $this->url . '/login',
            [
                'email' => $this->user->email,
                'password' => Str::random(10),
            ]
        )->assertStatus(HttpCodes::FORBIDDEN)
            ->assertJsonFragment([
                "success"=>false,
            ])
            ->assertJsonStructure([
                "success",
                "data" => [],
                "error" => ['0']
            ]);
        // incorrect email
        $this->post(
            $this->url . '/login',
            [
                'email' => $this->user->email . Str::random(1),
                'password' => $this->password,
            ]
        )->assertStatus(HttpCodes::FORBIDDEN)
            ->assertJsonFragment([
                "success"=>false,
            ])
            ->assertJsonStructure([
                "success",
                "data" => [],
                "error" => ['0']
            ]);
    }

    /**
     * check validations rules
     * /api/auth/login
     */
    public function testLoginValidatorRequestFailed()
    {
        // not valid email
        $this->post(
            $this->url . '/login',
            [
                'email' => Str::random(6),
                'password' => Str::random(6),
            ]
        )->assertStatus(HttpCodes::UNPROCESSABLE_ENTITY)
            ->assertJsonFragment([
                "success"=>false,
            ])
            ->assertJsonStructure([
                "success",
                "data" => [],
                "error" => ['email']
            ]);
        // without email
        $this->post(
            $this->url . '/login',
            [
                'password' => $this->password,
            ]
        )->assertStatus(HttpCodes::UNPROCESSABLE_ENTITY)
            ->assertJsonFragment([
                "success"=>false,
            ])
            ->assertJsonStructure([
                "success",
                "data" => [],
                "error" => ['email']
            ]);
        // short password
        $this->post(
            $this->url . '/login',
            [
                'email' => $this->user->email,
                'password' => Str::random(5),
            ]
        )->assertStatus(HttpCodes::UNPROCESSABLE_ENTITY)
            ->assertJsonFragment([
                "success"=>false,
            ])
            ->assertJsonStructure([
                "success",
                "data" => [],
                "error" => ['password']
            ]);
        // without password
        $this->post(
            $this->url . '/login',
            [
                'email' => $this->user->email,
            ]
        )->assertStatus(HttpCodes::UNPROCESSABLE_ENTITY)
            ->assertJsonFragment([
                "success"=>false,
            ])
            ->assertJsonStructure([
                "success",
                "data" => [],
                "error" => ['password']
            ]);

    }

    /**
     * success User get yourself
     * /api/auth/me
     */
    public function testMeRequestSuccess() {
        $this->post(
            $this->url . '/me',
            [],
            $this->headers
        )->assertStatus(HttpCodes::SUCCESS)
            ->assertJsonFragment([
                "success"=>true,
            ])
            ->assertJsonStructure([
                "success",
                "data" =>
                    ["id", "name","email", "email_verified_at"]
            ]);
    }

    /**
     * failed User get yourself
     * /api/auth/me
     */
    public function testMeRequestFailed() {
        // user not login
        $this->post(
            $this->url . '/me',
            []
        )->assertStatus(HttpCodes::FORBIDDEN)
            ->assertJsonFragment([
                "success"=>false,
            ])
            ->assertJsonStructure([
                "success",
                "data" => [],
                "error"=>["0"]
            ]);
    }

    /**
     * success User refresh token
     * /api/auth/refresh
     */
    public function testRefreshTokenRequestSuccess() {
        $this->post(
            $this->url . '/refresh',
            [],
            $this->headers
        )->assertStatus(HttpCodes::SUCCESS)
            ->assertJsonFragment([
                "success"=>true,
            ])
            ->assertJsonStructure([
                "success",
                "data" =>
                    ["access_token", "token_type","expires_in"]
            ]);
    }

    /**
     * failed User refresh token
     * /api/auth/login
     */
    public function testRefreshTokenRequestFailed() {
        // user not login
        $this->post(
            $this->url . '/refresh',
            []
        )->assertStatus(HttpCodes::FORBIDDEN)
            ->assertJsonFragment([
                "success"=>false,
            ])
            ->assertJsonStructure([
                "success",
                "data" => [],
                "error"=>["0"]
            ]);
    }

    /**
     * success User refresh token
     * /api/auth/refresh
     */
    public function testLogoutRequestSuccess() {
        $this->post(
            $this->url . '/logout',
            [],
            $this->headers
        )->assertStatus(HttpCodes::SUCCESS)
            ->assertJsonFragment([
                "success"=>true,
            ])
            ->assertJsonStructure([
                "success",
                "data" =>
                    ["message", ]
            ]);
    }

    /**
     * failed User logout
     * /api/auth/login
     */
    public function testLogoutRequestFailed() {
        // user not login
        $this->post(
            $this->url . '/refresh',
            []
        )->assertStatus(HttpCodes::FORBIDDEN)
            ->assertJsonFragment([
                "success"=>false,
            ])
            ->assertJsonStructure([
                "success",
                "data" => [],
                "error"=>["0"]
            ]);
    }

}
