<?php

namespace Tests;

use App\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected $user;
    protected $password;

    protected $headers = [
        'Content-Type' => 'application/json ',
        'X-Requested-With' => 'XMLHttpRequest '
    ];

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create([
            'password' => $this->password = 'secret',
        ]);
        $token = auth()->login($this->user);
        $this->setHeader([
            'Authorization' => 'Bearer ' . $token,
        ]);
    }

    /**
     * set header for request
     * @param array $headers
     */
    public function setHeader(array $headers)
    {
        $this->headers = array_merge($this->headers, $headers);
    }
}
