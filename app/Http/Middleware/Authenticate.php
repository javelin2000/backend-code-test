<?php

namespace App\Http\Middleware;

use App\Http\Responses\ErrorResponse;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Http\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Attempt to authenticate a user via the token in the request.
     * @param  Request  $request
     * @throws UnauthorizedHttpException
     * @return void
     */
    public function handle($request, Closure $next)
    {
        try{
            return parent::handle($request, $next);
        }catch (UnauthorizedHttpException $ex){
            return ErrorResponse::withThrowable($ex)->response();
        }
    }
}
