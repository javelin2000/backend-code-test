<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Auth;

/**
 * @group User
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    private $userRepository;

    /**
     * UserController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->middleware('auth:api', ['except' => ['store']]);
    }

    /**
     * create user
     *
     * @authenticated
     *
     * @bodyParam email string required The email of User. *[required; unique:users; email; max:255]* Example: sam@example.net
     * @bodyParam name string required The User name. *[required; string; max:255]* Example: sam
     * @bodyParam password string required The User password. *[required; string; min:6; max:255]* Example: secret
     *
     * @response {
     *    "success": true,
     *    "data": {
     *      "email": "sam@example.net",
     *      "name": "sam",
     *      "updated_at": "2019-08-06 05:56:59",
     *      "created_at": "2019-08-06 05:56:59",
     *      "id": 2
     *    }
     * }
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $userData = $request->all();
            $this->validator($userData)->validate();

            return (new SuccessResponse($this->userRepository->create($userData)))->response();
        } catch (\Exception $exception) {
            return ErrorResponse::withThrowable($exception)->response();
        }
    }

    /**
     * get user
     *
     * @authenticated
     *
     * @queryParam id required The User id. Example: 1
     *
     * @response {
     *    "success": true,
     *    "data": {
     *      "id": 1,
     *      "email": "tom@example.net",
     *      "name": "tom",
     *      "email_verified_at": null,
     *      "updated_at": "2019-08-06 05:56:59",
     *      "created_at": "2019-08-06 05:56:59"
     *    }
     * }
     *
     * @param $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $this->validator([], $id)->validate();

            return (new SuccessResponse($this->userRepository->getOne($id)))->response();
        } catch (\Exception $exception) {
            return ErrorResponse::withThrowable($exception)->response();
        }
    }

    /**
     * update user
     *
     * @authenticated
     *
     * @queryParam id required The User id. Example: 1
     * @bodyParam name string The User name. *[filled; string; max:255]* Example: jack
     * @bodyParam password string The User password. *[filled; string; min:6; max:255]* Example: secret
     *
     * @response {
     *    "success": true,
     *    "data": true
     * }
     *
     * @param Request $request
     * @param $id
     * @return SuccessResponse|JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $userData = $request->all();
            $this->validator($userData, $id)->validate();

            return (new SuccessResponse($this->userRepository->update($id, $userData)))->response();
        } catch (\Exception $exception) {
            return ErrorResponse::withThrowable($exception)->response();
        }
    }

    /**
     * delete user
     *
     * @authenticated
     *
     * @queryParam id required The User id. Example: 1
     *
     * @response {
     *    "success": true,
     *    "data": true
     * }
     *
     * @param $id
     * @return SuccessResponse|JsonResponse
     */
    public function destroy($id)
    {
        try {
            $this->validator([], $id)->validate();

            return (new SuccessResponse($this->userRepository->delete($id)))->response();
        } catch (\Exception $exception) {
            return ErrorResponse::withThrowable($exception)->response();
        }
    }

    /**
     * @param array $data
     * @param null $id
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data, $id = null)
    {
        $data['id'] = $id;
        return Validator::make($data, [
            'id' => 'nullable|in:' . Auth::id(),
            'email' => ($id ? 'in:' . Auth::user()->email : 'required|unique:users|email|max:255'),
            'name' => ($id ? '' : 'required|') . 'string|filled|max:255',
            'password' => ($id ? '' : 'required|') . 'string|filled|min:6|max:255'
        ]);
    }
}
