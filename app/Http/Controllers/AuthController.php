<?php

namespace App\Http\Controllers;

use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * @group Auth
 * Class AuthController
 * @package App\Http\Controllers
 */
class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * login
     * [Get a JWT token via given credentials.]
     *
     * @bodyParam email string required The User email. Example: tom@example.net
     * @bodyParam password string required The User password. Example: secret
     *
     * @response {
     *    "success": true,
     *      "data": {
     *          "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODA4MFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU2NTA3Nzg2NiwiZXhwIjoxNTY1MDgxNDY2LCJuYmYiOjE1NjUwNzc4NjYsImp0aSI6ImE4bFpBSldvSGNQYjNoOXEiLCJzdWIiOjEzMywicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.vMpbjrRX8RVfxtZq8QQ52MNghcLjjePMgF7Zr9K_hG4",
     *          "token_type": "bearer",
     *          "expires_in": 3600
     *      }
     * }
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        try {
            $credentials = $request->only('email', 'password');
            $this->validator($credentials)->validate();

            if ($token = $this->guard()->attempt($credentials)) {
                return $this->respondWithToken($token);
            }
            throw new UnauthorizedHttpException('jwt-auth', 'User not found');
        } catch (UnauthorizedHttpException $ex) {
            return ErrorResponse::withThrowable($ex)->response();
        }
    }

    /**
     * get yourself
     * [Get the authenticated User]
     *
     * @authenticated
     *
     * @response {
     *    "success": true,
     *    "data": {
     *      "id": 1,
     *      "email": "tom@example.net",
     *      "name": "tom",
     *      "email_verified_at": null,
     *      "updated_at": "2019-08-06 05:56:59",
     *      "created_at": "2019-08-06 05:56:59"
     *    }
     * }
     *
     * @return JsonResponse
     */
    public function me()
    {
        try {
            return (new SuccessResponse($this->guard()->user()))->response();
        } catch (\Exception $ex) {
            return ErrorResponse::withThrowable($ex)->response();
        }
    }

    /**
     * logout
     * [Log the user out (Invalidate the token)]
     *
     * @authenticated
     *
     * @response {
     *    "success": true,
     *    "data": {
     *      "message": "Successfully logged out"
     *    }
     * }
     *
     * @return JsonResponse
     */
    public function logout()
    {
        try {
            $this->guard()->logout();
            return (new SuccessResponse(['message' => 'Successfully logged out']))->response();
        } catch (\Exception $ex) {
            return ErrorResponse::withThrowable($ex)->response();
        }
    }

    /**
     * refresh token
     * [Refresh a token.]
     *
     * @authenticated
     *
     * @response {
     *    "success": true,
     *    "data": {
     *      "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9yZWZyZXNoIiwiaWF0IjoxNTY1MDI1NTIzLCJleHAiOjE1NjUwMzI1NTAsIm5iZiI6MTU2NTAyODk1MCwianRpIjoiRUFSY2djaGtCOXJiSWZLeiIsInN1YiI6MSwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.pKgXmSPCawjWRVInlR7HowSzzNvw3p9XLGN0J2_VAog",
     *      "token_type": "bearer",
     *      "expires_in": 3600
     *    }
     * }
     *
     * @return JsonResponse
     */
    public function refresh()
    {
        try {
            return $this->respondWithToken($this->guard()->refresh());
        } catch (\Exception $ex) {
            return ErrorResponse::withThrowable($ex)->response();
        }
    }

    /**
     * Get the token array structure.
     * @param  string $token
     * @return JsonResponse
     */
    protected function respondWithToken($token)
    {
        return (new SuccessResponse([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ]))->response();
    }

    /**
     * Get the guard to be used during authentication.
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard();
    }

    /**
     * @param array $data
     * @param null $id
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data, $id = null)
    {
        return Validator::make($data, [
            'email' => 'required|email|max:255',
            'password' => 'required|string|min:6|max:255'
        ]);
    }
}
