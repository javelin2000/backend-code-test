<?php

namespace App\Http\Responses;


use App\Constants\ErrorCodes;
use App\Constants\HttpCodes;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * Class ErrorResponse
 * @package App\Http\Responses
 *
 * Form new Throwable such way
 * message - error code from API
 * code - http status response for correct work of API
 *
 */
class ErrorResponse
{

    const EXCEPTION_FIELDS = ["password", "user_password", "pswd"];

    /**
     * @var \Throwable
     */
    private $throwable;

    private $error;
    private $code;
    private $data;

    private function __construct()
    {
    }

    public static function withThrowable(\Throwable $e): ErrorResponse
    {
        $self = new self();
        $self->throwable = $e;
        return $self;
    }

    public static function withData($error, int $code, $data = null): ErrorResponse
    {
        $self = new self();
        $self->error = $error;
        $self->code = $code;
        $self->data = $data;
        return $self;
    }

    public function response()
    {
        if ($this->throwable) {

            $error = null;
            $code = null;
            $data = [];
            if ($this->throwable instanceof UnauthorizedHttpException) {
                $error = [$this->throwable->getMessage()];
                $code = HttpCodes::FORBIDDEN;
                $level = 'warning';
            } else if ($this->throwable instanceof ModelNotFoundException) {
                $error = [ErrorCodes::NOT_FOUND];
                $code = HttpCodes::NOT_FOUND;
                $level = 'warning';
            } else if ($this->throwable instanceof ValidationException) {
                $error = $this->throwable->errors();
                $code = HttpCodes::UNPROCESSABLE_ENTITY;
                $level = 'warning';

            } else {
                $level = 'error';
                $error = [$this->throwable->getMessage()];
                if (env("APP_DEBUG", false)) {
                    $data["message"] = $this->throwable->getMessage() . " " . $this->throwable->getFile() . "( " . $this->throwable->getLine() . " ) " . $this->throwable->getTraceAsString();
                }
                $code = HttpCodes::SERVER_ERROR;
                if ($this->throwable->getCode() && $this->throwable->getCode() < 600 && $this->throwable->getCode() >= 400) {
                    $code = $this->throwable->getCode();
                }
            }
            $this->logInfo($level);
            return response()->json(['success' => false, 'error' => $error, "data" => $data], $code);
        } else {
            $error = is_string($this->error) ? [$this->error] : $this->error;
            $this->logInfo('warning');
            return response()->json(["success" => false, "error" => $error, "data" => $this->data], $this->code);
        }
    }

    private function logInfo($level)
    {
        /** @var Request $request */
        $request = app('request');
        $message = sprintf(" [%s] Request on (%s) %s : failed  - code : %s; message - %s; user - %s",
            (new \DateTime())->format("Y-m-d H:i:s"),
            $request->method(),
            $request->path(),
            ($this->throwable ? $this->throwable->getCode() : $this->code),
            ($this->throwable ? $this->throwable->getMessage() : json_encode($this->data)),
            (\Auth::user() ? \Auth::user()->id_user : "no User")
        );

        \Log::$level($message, [
            "user" => \Auth::user() ? \Auth::user()->id_user : null,
            "params" => $request->except(self::EXCEPTION_FIELDS),
            'exception' => $this->throwable,
            "data" => $this->data,
            "error" => $this->error,
            "code" => $this->code,
        ]);

    }

}
