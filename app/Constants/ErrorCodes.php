<?php

namespace App\Constants;


class ErrorCodes {

    const SUCCESS = "OK";
    const CREATED = "Created";

    const BAD_REQUEST = "Bad Request";
    const UNAUTHORIZED = "Unauthorized";
    const FORBIDDEN = "Forbidden";
    const NOT_FOUND = "Not Found";
    const UNPROCESSABLE_ENTITY = "Unprocessable Entity";

    const SERVER_ERROR = "Internal Server Error";
}


