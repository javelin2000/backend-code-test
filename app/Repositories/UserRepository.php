<?php


namespace App\Repositories;

use App\User;

class UserRepository
{
    public $user;

    /**
     * UserRepository constructor.
     * @param User $user
     */
    function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param int $id
     * @return User|null
     */
    public function getOne(int $id)
    {
        return User::find($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return User::create($data);
    }

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function update(int $id, array $data)
    {
        return User::find($id)->update($data);
    }

    /**
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        auth()->logout(true);
        return User::destroy($id);
    }
}
