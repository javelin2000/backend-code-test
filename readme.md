## Installation

1. install dependencies
```bash
    composer install
```
2. copy .env.example to .env
```bash
    cp .env.example .env
```
and edit follow fields: DB_DATABASE, DB_USERNAME, DB_PASSWORD
it must be same with MYSQL_DATABASE, MYSQL_USER, MYSQL_PASSWORD

3. start Docker
```bash
    docker-compose up -d
```

4. Make migrations 
login to the workspace container:
```bash
    docker exec -it backend-code-test_workspace_1 /bin/bash
```
a. generate APP key
```bash
    php artisan key:generate
```
b. create migrations and seed
```bash
    php artisan migrate --seed
```
c. Create JWT secret key  
```bash
    php artisan jwt:secret
```
Project ready to use 
by default project start on localhost:80 
you can change this port in variable NGINX_HOST_HTTP_PORT from .env:

* to start Tests: login to the workspace and use 
```bash
    ./vendor/bin/phpunit 
```
** to generate API docs: login to the workspace and use
```bash
    php artisan apidoc:generate
```
docs url: localhost:80/docs